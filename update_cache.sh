#!/bin/bash
set -euo pipefail

# expects the following variables defined in the environment:
# - S3_BUCKET: writable S3 bucket that will hold the tarballs
# - REPO_CONFIG: repository configuration ini file
# - CI_PROJECT_DIR: the full path where the job is run

REPO_TEMPDIR="${CI_PROJECT_DIR}/repo"

# Get repos from repo config
REPOS=$(crudini --get ${REPO_CONFIG} repos)

# Get repos that exist in S3 already.
REPOS_IN_S3=$(aws s3 ls ${S3_BUCKET} | awk '{print $NF}')

for REPO in $REPOS; do
    echo "${REPO}: Running update..."

    rm -rf ${REPO_TEMPDIR}
    mkdir -p ${REPO_TEMPDIR}
    GIT_URL=$(crudini --get ${REPO_CONFIG} repos ${REPO})

    # We store repositories as "owner.repo_name".
    REPO_NAME=$(basename -s .git $GIT_URL)
    OWNER=$(basename $(dirname $GIT_URL))
    REPO_TARBALL="${OWNER}.${REPO_NAME}.tar"
    REPO_REFS="${OWNER}.${REPO_NAME}.refs.md5"

    # Download the cached refs and compare it to current upstream
    echo "Checking for ${REPO_REFS} in S3..."
    REPO_REFS_MD5=$(git ls-remote --refs ${GIT_URL} | md5sum)
    if egrep "^${REPO_REFS}$" <<< "${REPOS_IN_S3}" > /dev/null; then
        REPO_REFS_MD5_OLD=$(aws s3 cp --no-progress ${S3_BUCKET}/${REPO_REFS} -)
    else
        REPO_REFS_MD5_OLD=nonexistent
    fi

    # Download the old cache and extract it.
    echo "Checking for ${REPO_TARBALL} in S3..."
    if ! egrep "^${REPO_TARBALL}$" <<< "${REPOS_IN_S3}" > /dev/null; then
        echo "${REPO}: Cached repo not found. Cloning repository..."
        git clone --mirror --quiet ${GIT_URL} ${REPO_TEMPDIR}
    elif [ "$REPO_REFS_MD5_OLD" != "$REPO_REFS_MD5" ]; then
        echo "${REPO}: Found cached repo. Updating cache..."
        aws s3 cp --no-progress ${S3_BUCKET}/${REPO_TARBALL} - | tar -xf - -C $REPO_TEMPDIR
        pushd $REPO_TEMPDIR
            git remote set-url origin ${GIT_URL}
            git fetch --all
        popd
    else
        echo "${REPO}: Found cached repo with no updates needed"
        continue
    fi

    # Stream the archive of the updated cache to S3. Compression saves maybe
    # 5-10% here but the CPU usage and time spent waiting isn't worth it.
    pushd $REPO_TEMPDIR
        echo "${REPO}: Packaging cache..."
        tar cf - . | aws s3 cp --no-progress - ${S3_BUCKET}/${REPO_TARBALL} --acl public-read
    popd
    rm -rf $REPO_TEMPDIR

    # Update refs in S3
    aws s3 cp --no-progress - ${S3_BUCKET}/${REPO_REFS} <<< "${REPO_REFS_MD5}"
done
